/**
 * 
 * Dado una cadena comprobar si es palíndromo en JavaScript
 * 
 */
/**
 * Primero creamos la función, definiendo que debe recibir un string.
 * Creamos la constante strReversed, en la que guardaremos el string revertido.
 * .split(''), lo separa en un array.
 * .reverse() revierte el arreglo.
 * .join('') Lo vuelve a convertir en string.
 * Lo que e echo es agregar una nueva constante que guarda el valor del string pero 
 * eliminando los espacios, gracias a la expresión regular que pasamos dentro del método 
 * .replace(), también lo convertimos en minúsculas.
 * Se crea a partir de el valor de 
 * newStr.
 * Por ultimo usando el operador ternario, indicamos que si ambas cadenas 
 * son iguales devuelva 'es palindromo', y si no 'no es palindromo'
 */

 function palindromeChecker(str) {
    const newStr = str.replace(/[\W_]/g, "").toLowerCase()
    const strReversed = newStr.split("").reverse().join("")
  
    return newStr === strReversed ? "es palindromo" : "no es palindromo"
  }
  
// Seleccione solo una de las tres para verificar y el resto comentelo

  console.log(palindromeChecker("Ali tomo tila")) // es palindromo
  console.log(palindromeChecker("Amad a la dama")) // es palindromo
  console.log(palindromeChecker("otra cosa")) // no es palindromo


// OTRA FORMA 
/*
function isPalindrome(Word){
  if(typeof Word === 'undefined' || Word === null){
      console.log("argument is undefined");
      return;
  }

  if(typeof Word === 'string'){

     Word = Word.replace(/\s/gi,"");

   if(Word.length < 1){
      console.log("wrong argument");
      return;
    }

    if(Word.length === 1){
      console.log("It's palindrome!");
      return;
    }

    for(var i=0;i<Math.floor(Word.length/2);i++){

      if(Word[i] !== Word[word.length - 1 - i]){
        console.log("It's not palindrome");
        return;
      }
    }
    console.log("It's palindrome");

  }else{

     console.log("argument must be a string");

  }

}

// Salida

isPalindrome("nurses run");
isPalindrome("abcdcba");
isPalindrome("hello world");
isPalindrome("  ");
isPalindrome(" ");
isPalindrome("");
isPalindrome(null);
isPalindrome();
isPalindrome(100);
isPalindrome({"name":"John"});
*/