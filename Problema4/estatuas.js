/**
 * A Pepito le entregaron un montón de estatuas de diferentes tamaños, Pepito quiere 
 * ordenarlas de la más pequeña a las más grande, sin embargo, también desea que cada estatua sea más 
 * grande que la anterior solo por 1 de diferencia, por lo que también ve pertinente adquirir nuevas estatuas. 
 * Ayúdala a determinar cuántas estatuas MAS debe adquirir para lograr su cometido.
 */

 /**
 * Método que ordena un array numeral usando el algoritmo de ordenación por burbuja
 * @param arrayDesordenado - Array 
 * @return Array 
 */
/*function ordenarPorBurbuja(arrayDesordenado) {
    // Copia el array recibido
    var tempArray = arrayDesordenado;
    var volverAOrdenar = false;
    // Recorre el array
    tempArray.forEach(function (valor, key) {
        // Comprueba si el primero es mayor que el segundo y no esta en la última posición
        if (tempArray[key] > tempArray[key + 1] && tempArray.length - 1 != key) {
            // Intercambia la primera posición por la segunda
            var primerNum = tempArray[key];
            var segundoNum = tempArray[key + 1];
            tempArray[key] = segundoNum;
            tempArray[key + 1] = primerNum;
            // Si debe volver a ordenarlo
            volverAOrdenar = true;
        }
    });
    // Vuelve a llamar al función
    if (volverAOrdenar) {
        ordenarPorBurbuja(tempArray);
    }
    // Array ordenado
    return tempArray;
}
ordenarPorBurbuja([11, 6, 5, 1, 12, 14, 118, 2, 3]);*/

// Prueba

 /**
  * Entrada:
  * 
  * [11, 6, 5, 1, 12, 14, 118, 2, 3]
  * 
  
  * Salida:
  * 
  * [1, 2, 3, 5, 6, 11, 12, 14, 118]
  * 
  */


 // OTRA FORMA PROBAR EN CONSOLA JavaScript (jsbin) RUN && CLEAR 

 // Donde los name seran los nombres de mis ESTATUAS && level los tamños o cometido que indica en el enunciado

 // sort 
var pokemons = [
    { name:'pickachu', level: 50 } , 
    { name:'bulbasour', level: 10 } , 
    { name:'snorlax', level: 1 } , 
    { name:'mew', level: 2 } , 
    { name:'abra', level: 150 } 
  ];
  // var numbers = [2, 34, 10, 2, 5, 1];
  console.log(pokemons.sort(function (prev, next) {
    if (prev.name > next.name) {
      return 1;
    }
    if (prev.name < next.name) {
      return -1;
    }
    return 0;
  //  return prev.level - next.level;
  }));

  // 

  /**
   * Salida: en Console jsbin
   * 
   * Ordenado si nos fijamos los nombres estan ordenados de forma alfavetica por la 1ra letra de cada nombre
   * abra, bulbasour, mew, pickachu, snorlax
   * 
    [[object Object] {
    level: 150,
    name: "abra"
    }, [object Object] {
    level: 10,
    name: "bulbasour"
    }, [object Object] {
    level: 2,
    name: "mew"
    }, [object Object] {
    level: 50,
    name: "pickachu"
    }, [object Object] {
    level: 1,
    name: "snorlax"
    }]
   * 
   */


    // PROBAR ESTA OTRA FORMA PARA lavel = tamaño ORDENADO DE FORMA ASCENDENTE comentando los if

   // sort 
var pokemons = [
    { name:'pickachu', level: 50 } , 
    { name:'bulbasour', level: 10 } , 
    { name:'snorlax', level: 1 } , 
    { name:'mew', level: 2 } , 
    { name:'abra', level: 150 } 
  ];
  // var numbers = [2, 34, 10, 2, 5, 1];
  console.log(pokemons.sort(function (prev, next) {
  //  if (prev.name > next.name) {
    //  return 1;
    //}
    //if (prev.name < next.name) {
      //return -1;
    //}
    //return 0;
    return prev.level - next.level;
  }))
  
    /**
   * Salida: en Console jsbin
   * 
   * No olvidemos comentar los if como se muestra arriba
   * 
   * Ordenado si nos fijamos los level (tamaño) estan ordenados de forma acendente 1, 2, 10, 50, 150
   * 
    [[object Object] {
    level: 1,
    name: "snorlax"
    }, [object Object] {
    level: 2,
    name: "mew"
    }, [object Object] {
    level: 10,
    name: "bulbasour"
    }, [object Object] {
    level: 50,
    name: "pickachu"
    }, [object Object] {
    level: 150,
    name: "abra"
    }]
   * 
   */