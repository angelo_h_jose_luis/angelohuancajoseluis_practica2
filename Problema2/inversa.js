/**
 * 
 * Dado una cadena mostrar la inversa en JavaScript
 * 
 */

function reverseString(s) {
  // Funcion reverseString(s) => string
  //  s: string que deseamos invertir
  //  devuelve: String
  
      const len = s.length
      // verificamos si el tamaño del string es par o impar
      const isOdd = len % 2;
      // calculamos la mitad del string para poder iterar solo hasta alli (esto es por mejorar un poco la eficiencia)
      // si es impar debemos redondear (redondeo por defecto o redondeo hacia abajo) 
      // eso significa que el carácter del medio (central) no cambia.
      const middle = isOdd ? Math.floor(len / 2) : len / 2;
  
      // Ahora guardamos el carácter central si el tamaño del string es impar,
      // en caso que sea par, no existe carácter central
      let center = isOdd ? s[middle] : undefined;
      let head = '';
      let tail = '';
      for (let i = 0 ; i < middle ; i++) {
          // Aqui básicamente vamos llenando la cabezera y la cola de forma invertida.
          // Leemos el string original de atrás hacia adelante y lo vamos almacenando.
          // Nótese que 'head' se llena desde el final hasta la mitad y 'tail' se llena
          // desde la mitad hasta el inicio.
          
          head = head.concat(s[len - (i + 1)]);
          tail = tail.concat(s[middle - (i + 1)]);
      } // fin bucle for
  
      // Ahora podemos construir nuestro string invertido usando head, center y tail
      const str = center ? head.concat(center.concat(tail)) : head.concat(tail); 
      // Finalmente devolvemos el resultado
      return str;
  } // fin funcion
  
  // Un par de pruebas usando tamaños par e impar:
  let reversedString = '';
  const evenString = 'QWERTY'; // Primera prueba
  const oddString = 'QWERTYU' // Segunda prueba
  
  reversedString = reverseString(evenString);
  console.log(evenString);
  console.log(reversedString);
  
  reversedString = reverseString(oddString);
  console.log(oddString);
  console.log(reversedString);

  /**
  * Entrada:
  * 
  * QWERTY
  * QWERTYU
  * 
  * Salida:
  * 
  * YTREWQ
  * UYTREWQ
  * 
  */ 

  // OTRA FORMA ACTIVA PARA PROBAR (O PUEDE DESCOMENTAR /* */)

  // reverseString 1
 
/*function reverseString(string) {
  var result = '';
  for (var i = string.length - 1 ; i >= 0; i--) {
    result += string[i];
  }
  return result;
}
// reverseString 2

function reverseString(string) {
  return string.split('').reverse().join('');
}
// reverseString 3 es6
//const reverseString = (string) => Array.from(string).reduce((a,e) => e+a);

// test cases
console.log(assert(reverseString('hola'),'aloh')); // Primera prueba
console.log(assert(reverseString('hola mundo'),'odnum aloh')); // Segunda prueba
console.log(assert(reverseString('h'),'h')); // Tersera prueba

function assert(a, b) {
  console.log(a,b)
  return a === b;
}*/